using System;

namespace BudgetApplication
{
    public class Expense : ITransaction
    {
        public ICurrencyAmount Amount { get; }
        public DateTimeOffset Date { get; }
        private string Category { get; }
        private string Destination { get; }

        public Expense(ICurrencyAmount amount, DateTimeOffset date, string category, string destination)
        {
            Date = date;
            Amount = amount;
            Category = category;
            Destination = destination;
        }
        
        public override string ToString() => $"Трата {Amount} в {Destination} по категории {Category}";

        public string ToFileString() => $"Трата {Amount} {Category} {Destination}";
    }
}