﻿using System;
using System.Net.Http;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Caching.Memory;

namespace BudgetApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            var currencyConverter = new ExchangeRatesApiConverter(new HttpClient(), new MemoryCache(new MemoryCacheOptions()), "be0955a52910f1d3ab85d3eb7a9a1768");
            var transactionParser = new TransactionParser();
            var transactionRepository = new InFileTransactionRepository(transactionParser);
            var budgetApp = new BudgetApplication(currencyConverter, transactionRepository, transactionParser);
            
            var stop = false;
            while (!stop)
            {
                Console.WriteLine("BudgetApplication");
                Console.WriteLine("Choose one of the actions:");
                Console.WriteLine("1. Add transaction");
                Console.WriteLine("2. Show balance in currency");
                Console.WriteLine("3. Show all transactions");
                Console.WriteLine("0. Exit");
                var userAction = Console.ReadLine();
                switch (userAction)
                {
                    case ("1"):
                        Console.WriteLine("Enter transaction:");
                        var userTransaction = Console.ReadLine();
                        budgetApp.AddTransaction(userTransaction);
                        break;
                    case ("2"):
                        Console.WriteLine("Enter currency:");
                        var userCurrency = Console.ReadLine();
                        budgetApp.OutputBalanceInCurrency(userCurrency);
                        break;
                    case ("3"):
                        budgetApp.OutputTransactions();
                        break;
                    case ("0"):
                        stop = true;
                        break;
                }
            }
        }
    }
}
